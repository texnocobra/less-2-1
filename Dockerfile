FROM python:3.7.8

ENV PYTHONUNBUFFERED=1

WORKDIR /Blog

RUN python -m pip install --upgrade pip

COPY requirements.txt requirements.txt

RUN python -m pip install -r requirements.txt

COPY . .
